-- liquibase formatted sql

-- changeset palvarado:1644434150681-1
CREATE TABLE "public"."requerimientos" ("id" SERIAL NOT NULL, "nro_req" VARCHAR(25) NOT NULL, "fecha_sol" TIMESTAMP WITHOUT TIME ZONE NOT NULL, "nomb_comp" VARCHAR(250) NOT NULL, "descrip_req" VARCHAR(25), "justific" TEXT NOT NULL, "cod_u_rbl" VARCHAR(25) NOT NULL, "observ" TEXT, "arch_adj" TEXT, "estado" VARCHAR(25) NOT NULL, "open_project_id" VARCHAR(25), "open_project_identifier" VARCHAR(250), "open_project_title" VARCHAR(250), "open_project_status" VARCHAR(25), "open_project_percentage_done" numeric, "open_project_assignee" VARCHAR(250), "open_project_responsible" VARCHAR(250), "open_project_priority" VARCHAR(25), "created_at" TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(), "updated_at" TIMESTAMP WITHOUT TIME ZONE, "username" VARCHAR(25) DEFAULT '"session_user"()', CONSTRAINT "requerimientos_pkey" PRIMARY KEY ("id"));

-- changeset palvarado:1644434150681-2
CREATE TABLE "public"."incidentes" ("id" SERIAL NOT NULL, "nro_inc" VARCHAR(25) NOT NULL, "fecha_sol" TIMESTAMP WITHOUT TIME ZONE NOT NULL, "nomb_comp" VARCHAR(250) NOT NULL, "arch_adj" TEXT, "observ" TEXT, "descrip" TEXT, "estado" VARCHAR(25) NOT NULL, "cod_u_rbl" VARCHAR(25) NOT NULL, "open_project_id" VARCHAR(25), "open_project_identifier" VARCHAR(250), "open_project_title" VARCHAR(250), "open_project_status" VARCHAR(25), "open_project_percentage_done" numeric, "open_project_assignee" VARCHAR(250), "open_project_responsible" VARCHAR(250), "open_project_priority" VARCHAR(25), "created_at" TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(), "updated_at" TIMESTAMP WITHOUT TIME ZONE, "username" VARCHAR(25) DEFAULT '"session_user"()', CONSTRAINT "incidentes_pkey" PRIMARY KEY ("id"));

-- changeset palvarado:1644434150681-3
CREATE TABLE "public"."comments" ("id" SERIAL NOT NULL, "tabla" VARCHAR, "tabla_id" INTEGER, "comment" TEXT, "created_at" TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(), "updated_at" TIMESTAMP WITHOUT TIME ZONE, "username" VARCHAR(25) DEFAULT '"session_user"()', CONSTRAINT "comments_pkey" PRIMARY KEY ("id"));

-- changeset palvarado:1644434150681-4
CREATE TABLE "public"."table1" ("id" SERIAL NOT NULL, "description" VARCHAR(50), CONSTRAINT "table1_pkey" PRIMARY KEY ("id"));

-- changeset palvarado:1644434150681-5
CREATE TABLE "public"."table2" ("id" SERIAL NOT NULL, "description" VARCHAR(50), CONSTRAINT "table2_pkey" PRIMARY KEY ("id"));

-- changeset palvarado:1644434150681-6
ALTER TABLE "public"."requerimientos" ADD CONSTRAINT "requerimientos_nro_req_open_project_id_key" UNIQUE ("nro_req", "open_project_id");

-- changeset palvarado:1644434150681-7
ALTER TABLE "public"."incidentes" ADD CONSTRAINT "incidentes_nro_inc_open_project_id_key" UNIQUE ("nro_inc", "open_project_id");

