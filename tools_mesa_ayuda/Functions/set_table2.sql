-- set_table2.sql
DROP FUNCTION IF EXISTS public.set_table2(integer, varchar(50));
CREATE OR REPLACE FUNCTION public.set_table2 (
 p_id integer,   
 p_description varchar(50) 
)
RETURNS text AS
$body$
DECLARE 
v_id integer;
BEGIN
PERFORM * FROM table2 WHERE id = p_id;
IF FOUND THEN
    update table2 set description = p_description WHERE id = p_id;
    RETURN p_id::text;
END IF;
INSERT INTO table2 (description) VALUES (p_description) RETURNING id INTO v_id;
raise notice 'cambios con liquibase y git hooks';
RETURN v_id::text;

EXCEPTION
    WHEN SQLSTATE 'P9999' THEN RETURN SQLERRM;
    WHEN OTHERS THEN RETURN SQLERRM;

END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
PARALLEL UNSAFE
COST 100;