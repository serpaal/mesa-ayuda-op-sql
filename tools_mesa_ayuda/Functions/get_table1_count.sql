-- get_table1_count.sql
CREATE OR REPLACE FUNCTION public.get_table1_count (
 p_num integer default 1
)
RETURNS text AS
$body$
DECLARE 
BEGIN
IF p_num = 1 THEN
 update table1 set descripcion = 'actualizacion desde window' where id = 1;
ELSE 
 INSERT INTO tabla1(description)
 VALUES ('R3-update-get-table1 from ubuntu');
END IF;
RETURN '1';
EXCEPTION
    WHEN SQLSTATE 'P9999' THEN RETURN SQLERRM;
    WHEN OTHERS THEN RETURN SQLERRM;

END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
PARALLEL UNSAFE
COST 100;