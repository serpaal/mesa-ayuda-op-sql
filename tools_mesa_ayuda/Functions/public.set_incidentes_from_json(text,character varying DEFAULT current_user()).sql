CREATE OR REPLACE FUNCTION public.set_incidentes_from_json (
  p_incidentes_json text,
  p_username varchar = "current_user"()
)
RETURNS text AS
$body$
DECLARE
    v_cursor_incidentes refcursor;
	v_record_incidente record;
    v_result text;
    v_response text;
    v_exception_context text;
	v_exception_detail text;
BEGIN
    -- comentario introducido en branch R1/calcular-tiempos de ejecucion
    OPEN v_cursor_incidentes FOR SELECT r.*
    FROM (
      WITH incidente AS (
        select
        (e #>> '{nro_inc}')::VARCHAR(25) as nro_inc,
        (e #>> '{fecha_sol}')::TIMESTAMP as fecha_sol,
        (e #>> '{nomb_comp}')::VARCHAR(250) as nomb_comp,
        (e #>> '{arch_adj}')::TEXT as arch_adj,
        (e #>> '{observ}')::TEXT as observ,
        (e #>> '{descrip}')::TEXT as descrip,
        (e #>> '{cod_u_rbl}')::VARCHAR(25) as cod_u_rbl,
        (e #>> '{estado}')::VARCHAR(25) as estado
        from
        jsonb_array_elements(p_incidentes_json::jsonb) e
      )
      select * from incidente
	) r ORDER BY r.fecha_sol ASC;

 -- loop incidentes
	LOOP
    	FETCH v_cursor_incidentes INTO v_record_incidente;
    	EXIT WHEN NOT FOUND;
        IF NOT EXISTS (SELECT * FROM incidentes WHERE nro_inc = v_record_incidente.nro_inc::varchar(25)) THEN
        	INSERT INTO incidentes (
            	nro_inc,
               	fecha_sol,
               	nomb_comp,
                arch_adj,
                observ,
               	descrip,
               	cod_u_rbl,
              	estado,
                username
             )VALUES(
                v_record_incidente.nro_inc::varchar(25),
                v_record_incidente.fecha_sol::TIMESTAMP,
                v_record_incidente.nomb_comp::varchar(250),
                v_record_incidente.arch_adj::text,
                v_record_incidente.observ::varchar(250),
                v_record_incidente.descrip::text,
                v_record_incidente.cod_u_rbl::varchar(25),
                v_record_incidente.estado::varchar(25),
                p_username
             );
        END IF;
    END LOOP;
    CLOSE v_cursor_incidentes;

 RETURN '1';
EXCEPTION
    WHEN SQLSTATE 'P9999' THEN RETURN SQLERRM;
    WHEN OTHERS THEN
    RAISE NOTICE 'SE HA GENERADO UNA EXCEPTION %', SQLERRM;
    		GET STACKED DIAGNOSTICS v_exception_context = PG_EXCEPTION_CONTEXT,
									v_exception_detail = PG_EXCEPTION_DETAIL;
            RETURN SQLERRM;

END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
PARALLEL UNSAFE
COST 100;

ALTER FUNCTION public.set_incidentes_from_json (p_incidentes_json text, p_username varchar)
  OWNER TO postgres;