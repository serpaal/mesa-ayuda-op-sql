-- set_table1.sql
DROP FUNCTION IF EXISTS public.set_table1(integer, varchar(50));
CREATE OR REPLACE FUNCTION set_table1 (
 p_id integer,
 p_description varchar(50)
)
RETURNS text AS
$body$
DECLARE 
v_id integer;
BEGIN
PERFORM * from table1 where id = p_id;
IF found THEN
    update table1 set description = p_description where id = p_id;
    return p_id::text;
END IF;
INSERT INTO table1(description)
VALUES (p_description) RETURNING id INTO v_id;

RETURN v_id::text;
EXCEPTION
    WHEN SQLSTATE 'P9999' THEN RETURN SQLERRM;
    WHEN OTHERS THEN RETURN SQLERRM;

END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
PARALLEL UNSAFE
COST 100;