-- table1.sql
CREATE TABLE table1 (
  id   serial primary KEY,
  description VARCHAR(50)
);

--INSERT INTO table1 (description) VALUES ('Description for item');