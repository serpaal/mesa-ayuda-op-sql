-- table2.sql
CREATE TABLE table2 (
  id   serial primary KEY,
  description VARCHAR(50)
);

INSERT INTO table2 (description) VALUES ('Se ha creado la tabla table2');